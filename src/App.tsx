import React from 'react';
import './App.css';
import Day from './Day07';

const App: React.FC = () => {
  return (
    <div className="App">
      <Day />
    </div>
  );
}

export default App;
