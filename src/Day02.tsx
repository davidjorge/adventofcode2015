import * as helpers from './helpers';
import React from 'react'

const day = '02';

interface DayState {
	squareFeet: number,
	ribbonLength: number
}

export default class Day extends React.Component {

	state: DayState = {
		squareFeet: 0,
		ribbonLength: 0
	};

	render() {
		return (
			<div>
				<div>Part 1: Total Square Feet {this.state.squareFeet}</div>
				<div>Part 2: Ribbon Length {this.state.ribbonLength}</div>
			</div>
		);
	}

	componentDidMount() {
		this.part1andPart2();
	}

	private calcFeet(length: number, width: number, height: number): number {
		// 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.
		let feet = 2 * length * width + 2 * width * height + 2 * height * length;		
		if (length >= width && length >= height) {
			feet += width * height;
		} else if (width >= length && width >= height) {
			feet += length * height;
		} else if (height >= width && height >= length) {
			feet += width * length;
		}
		return feet;
	}

	private calcRibbon(length: number, width: number, height: number): number {		
		let bow = length * width * height;
		if (length >= width && length >= height) {
			bow += 2*width + 2*height;
		} else if (width >= length && width >= height) {
			bow += 2*length + 2*height;
		} else if (height >= width && height >= length) {
			bow += 2*width + 2*length;
		}
		return bow;
	}

	private parseLine(line: string): [number, number, number] {
		const sides = line.split('x').map( s => parseInt(s));
		if (sides.length !== 3) {
			console.log('ERROR: Should be three sides');
		}
		return [sides[0], sides[1], sides[2]];
	}
	// https://adventofcode.com/2015/day/02
	async part1andPart2() {
		const lines = await helpers.readLines(day)
		let feet = 0;
		let ribbon = 0;
		lines.forEach(line => {
			const [length, width, height] = this.parseLine(line);			
			feet += this.calcFeet(length, width, height);
			ribbon += this.calcRibbon(length, width, height);
		});
		this.setState({ squareFeet: feet, ribbonLength: ribbon });
	}
}