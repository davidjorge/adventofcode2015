import * as helpers from './helpers';
import React from 'react'

const day = '07';

interface Sentence {
	var: string
	op1: string
	op2: string | undefined
	js: string
}

type RegExpAndReplaceStr = [RegExp, string]
const InsRegExp: { [key: string]: RegExpAndReplaceStr } = {
	and: [/(\w+) AND (\w+) -> (\w+)/ig, "$3 = $1 & $2;"], // af AND ah -> ai
	or: [/(\w+) OR (\w+) -> (\w+)/ig, "$3 = $1 | $2;"], // af OR ah -> ai
	not: [/NOT (\w+) -> (\w+)/ig, "$2 = 65535 - $1;"], // NOT lk -> ll
	lshift: [/(\w+) LSHIFT (\d+) -> (\w+)/ig, "$3 = $1 << $2;"],  // eo LSHIFT 15 -> es
	rshift: [/(\w+) RSHIFT (\d+) -> (\w+)/ig, "$3 = $1 >> $2;"], // eo RSHIFT 15 -> es
	assign: [/(\w+) -> (\w+)/ig, "$2 = $1;"]  // 0 -> c
}

class Parser {
	convertInstruction(line: string): Sentence {
		const names = [InsRegExp.and, InsRegExp.or, InsRegExp.not, InsRegExp.lshift, InsRegExp.rshift, InsRegExp.assign];
		const [regExp, replaceStr]: RegExpAndReplaceStr = names.find(([regExp, _]) => regExp.test(line))!;
		const js = line.replace(regExp, replaceStr);
		const matchIterator = line.matchAll(regExp);
		const matches = [...matchIterator][0];
		if (matches.length === 4) {
			return { js: js, var: matches[3], op1: matches[1], op2: matches[2] };
		} else if (matches.length === 3) {
			return { js: js, var: matches[2], op1: matches[1], op2: undefined };
		} else {
			throw new Error('Error parsing line');
		}
	}
}

interface DayState {
	sentences: Sentence[]
}

export default class Day extends React.Component {

	state: DayState = {
		sentences: []
	};

	render() {
		const code = this.state.sentences.map((sentence, index) => (<div key={index}>{sentence.js} ÑÑ {sentence.var} = {sentence.op1} {sentence.op2}</div>));
		return (
			<div style={ {textAlign: "left"} }>{code}</div>
		);
	}

	componentDidMount() {
		this.part1();
		// this.part2();
	}

	// https://adventofcode.com/2015/day/05
	async part1() {
		// Answered 568658 (too low)
		const lines = await helpers.readLines(day, true);
		const parser = new Parser();
		const sentences = lines.map(line => parser.convertInstruction(line));
		this.setState({ sentences: sentences });
	}

	// https://adventofcode.com/2015/day/06
	async part2() {
	}
}