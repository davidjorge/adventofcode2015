import * as helpers from './helpers';
import React from 'react'

const day = '03';

class Position2D {
	x = 0;
	y = 0;

	constructor(x: number, y: number) { this.x = x; this.y = y; }
	get toKey(): string { return this.x + ',' + this.y; }
};

interface DayState {
	housesVisited: number
	housesVisitedWithRobo: number
}

export default class Day extends React.Component {

	santaPosition = new Position2D(0, 0);
	roboPosition = new Position2D(0, 0);
	housesVisited: { [pos: string]: number } = { "0,0": 1 };

	state: DayState = {
		housesVisited: 0,
		housesVisitedWithRobo: 0
	};

	render() {
		return (
			<div>
				<div>Part 1: Houses Visited {this.state.housesVisited}</div>
				<div>Part 2: Total Houses Visited {this.state.housesVisitedWithRobo}</div>
			</div>
		);
	}

	componentDidMount() {
		this.part1();
	}

	private updatePosition(movement: string, position: Position2D) {
		switch (movement) {
			case "^": position.y += 1; break;		
			case "v": position.y -= 1; break;		
			case "<": position.x -= 1; break;		
			case ">": position.x += 1; break;		
			default:
				console.log('Unknown input', movement);break;
		}
	}

	// https://adventofcode.com/2015/day/03
	async part1() {
		this.santaPosition = new Position2D(0, 0);
		this.roboPosition = new Position2D(0, 0);
		this.housesVisited = { "0,0" : 1 };

		const input = await helpers.readInput(day)
		for (const movement of input) {
			this.updatePosition(movement, this.santaPosition);
			let visitedTimes = this.housesVisited[this.santaPosition.toKey];
			visitedTimes = (visitedTimes ?? 0) + 1;
			this.housesVisited[this.santaPosition.toKey] = visitedTimes;
		}

		const visited = Object.values(this.housesVisited).length;
		this.setState({ housesVisited: visited });

		this.part2();
	}

	// https://adventofcode.com/2015/day/03
	async part2() {
		this.santaPosition = new Position2D(0, 0);
		this.roboPosition = new Position2D(0, 0);
		this.housesVisited = { "0,0": 1 };
		
		const input = await helpers.readInput(day)
		this.housesVisited = { "0,0" : 1 };
		let turnSanta = true;
		for (const movement of input) {
			let position: Position2D;
			if (turnSanta) {
				this.updatePosition(movement, this.santaPosition);
				position = this.santaPosition;
			} else {
				this.updatePosition(movement, this.roboPosition);
				position = this.roboPosition;
			}
			turnSanta = !turnSanta;
			let visitedTimes = this.housesVisited[position.toKey];
			visitedTimes = (visitedTimes ?? 0) + 1;
			this.housesVisited[position.toKey] = visitedTimes;
		}

		const visited = Object.values(this.housesVisited).length;
		this.setState({ housesVisitedWithRobo: visited });
	}
}