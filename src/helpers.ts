export async function readInput(day: string, test: boolean = false): Promise<string> {
  const url = `./inputs/Day${day}${test ? '_test' : ''}.txt`;
  console.log(url);
  const response = await fetch(url);
  return response.text();
}

export async function readLines(day: string, test: boolean = false): Promise<string[]> {
  const url = `./inputs/Day${day}${test ? '_test' : ''}.txt`;
  const response = await fetch(url);
  const text = await response.text();  
  return text.split('\n');
}
