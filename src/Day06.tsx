import * as helpers from './helpers';
import React from 'react'

const day = '06';

interface DayState {
}

enum Command {
	turnOn = 'turn on',
	turnOff = 'turn off',
	toggle = 'toggle'
}

interface Coordinates {
	x: number;
	y: number;
}

interface Sentence {
	command: Command
	start: Coordinates
	end: Coordinates
}

enum WordParsed {
	turn = 'turn',
	on = 'on',
	off = 'off',
	toggle = 'toggle',
	through = 'through'
}

class Parser {
	words: string[] = [];
	nextWordIndex = 0;

	private parseCommand(): Command {
		let command: Command = Command.toggle;

		if (this.words[0] === WordParsed.turn) {
			if (this.words[1] === WordParsed.on) {
				command = Command.turnOn;
			} else if (this.words[1] === WordParsed.off) {
				command = Command.turnOff;
			}
			this.nextWordIndex = 2;
		} else if (this.words[0] === WordParsed.toggle) {
			command = Command.toggle;
			this.nextWordIndex = 1;
		}
		return command;
	}

	private parseCoordinates(): Coordinates {
		const [xStr, yStr] = this.words[this.nextWordIndex].split(',');
		this.nextWordIndex += 1;
		return { x: parseInt(xStr), y: parseInt(yStr) };
	}

	parseLine(line: string): Sentence {
		this.words = line.split(' ');
		const command = this.parseCommand();
		const start = this.parseCoordinates();
		if (this.words[this.nextWordIndex] !== WordParsed.through) {
			console.log('Error parsing line', line);
			throw new Error('Could not parse line ' + line);
		}
		this.nextWordIndex += 1;
		const end = this.parseCoordinates();
		return { command: command, start: start, end: end };
	}

}

type StatusPt1 = boolean;
type Status = number;

class Panel {
	lights: Status[][] = new Array(1000);
	width = 1000;
	height = 1000;

	constructor() {
		for (let x = 0; x < this.width; x++) {
			this.lights[x] = new Array(1000);
			for (let y = 0; y < this.height; y++) {
				this.lights[x][y] = 0;
			}
		}
	}

	executeCommand(command: Command, start: Coordinates, end: Coordinates, part1: boolean = false) {
		if (part1) {
			switch (command) {
				case Command.toggle: this.togglePt1(start, end); break;
				case Command.turnOn: this.turnOnPt1(start, end); break;
				case Command.turnOff: this.turnOffPt1(start, end); break;
			}
		} else {
			switch (command) {
				case Command.toggle: this.toggle(start, end); break;
				case Command.turnOn: this.turnOn(start, end); break;
				case Command.turnOff: this.turnOff(start, end); break;
			}
		}
	}

	countLightsOn() {
		let onCounter = 0;
		for (let y = 0; y < this.height; y++) {
			for (let x = 0; x < this.width; x++) {
				if (this.lights[x][y] > 0) {
					onCounter += 1;
				}
			}
		}
		return onCounter;
	}

	countBrightness() {
		let brightnessCounter = 0;
		for (let y = 0; y < this.height; y++) {
			for (let x = 0; x < this.width; x++) {
				brightnessCounter += this.lights[x][y];
			}
		}
		return brightnessCounter;
	}

	private executeInLeds(start: Coordinates, end: Coordinates, command: (status: Status) => Status) {
		for (let y = start.y; y <= end.y; y++) {
			for (let x = start.x; x <= end.x; x++) {
				this.lights[x][y] = command(this.lights[x][y]);
			}
		}
	}

	private togglePt1(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (status) => status === 1 ? 0 : 1);
	}

	private turnOnPt1(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (_) => 1);
	}

	private turnOffPt1(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (_) => 0);
	}

	private toggle(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (status) => status + 2);
	}

	private turnOn(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (status) => status + 1);
	}

	private turnOff(start: Coordinates, end: Coordinates) {
		this.executeInLeds(start, end, (status) => Math.max(status - 1, 0));
	}
}

export default class Day extends React.Component {

	state: DayState = {
	};

	render() {
		return (
			<div>
				<div>Result</div>
			</div>
		);
	}

	componentDidMount() {
		this.part1();
		this.part2();
	}

	// https://adventofcode.com/2015/day/05
	async part1() {
		// Answered 568658 (too low)
		const lines = await helpers.readLines(day, false);
		const parser = new Parser();
		const panel = new Panel();
		lines.forEach(line => {
			const sentence = parser.parseLine(line);
			panel.executeCommand(sentence.command, sentence.start, sentence.end, true);
		});
		console.log('Lights on part 1', panel.countLightsOn());
	}

	// https://adventofcode.com/2015/day/06
	async part2() {
		// Answered 568658 (too low), 63613673198 (too high)
		const lines = await helpers.readLines(day, false);
		const parser = new Parser();
		const panel = new Panel();
		lines.forEach(line => {
			const sentence = parser.parseLine(line);
			panel.executeCommand(sentence.command, sentence.start, sentence.end);
		});
		console.log('Brightness part 2', panel.countBrightness());
	}
}