import * as helpers from './helpers';
import React from 'react'

const day = '01';

interface DayState {
	floor: number
	positionBasement: number
}

export default class Day extends React.Component {

	state: DayState = {
		floor: 0,
		positionBasement: 0
	};

	render() {
		return (			
			<div>
				<div>Part 1: Final floor {this.state.floor}</div>
				<div>Part 2: Position at basement {this.state.positionBasement}</div>
			</div>
		);
	}

	componentDidMount() {
		this.part1();
		this.part2();
	}

	// https://adventofcode.com/2015/day/01
	async part1() {
		const input = await helpers.readInput(day);
		let floor = 0;
		for (const letter of input) {
			if (letter === '(') {
				floor += 1;
			} else if (letter === ')') {
				floor -= 1;
			}
		}
		this.setState( {floor: floor});
	}

	// https://adventofcode.com/2019/day/3#part2
	async part2() {
		const input = await helpers.readInput(day);
		let floor = 0;
		let position = 0;
		for (const letter of input) {
			if (letter === '(') {
				floor += 1;
			} else if (letter === ')') {
				floor -= 1;
			}
			position += 1;			
			if (floor === -1) {
				this.setState({ positionBasement: position });
				break;
			}
		}
	}
}