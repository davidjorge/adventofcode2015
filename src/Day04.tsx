import React from 'react'
import { Md5 } from "md5-typescript";

interface DayState {
	md5Number: number
}

export default class Day extends React.Component {

	state: DayState = {
		md5Number: 0
	};

	render() {
		return (
			<div>
				<div>Part 1: MD5 {this.state.md5Number}</div>
				{/* <div>Part 2: Total Houses Visited {this.state.housesVisitedWithRobo}</div> */}
			</div>
		);
	}

	componentDidMount() {
		this.part1();
	}
	// https://adventofcode.com/2015/day/04
	async part1() {
		const input = "iwrupvqb";
		let found = false;
		let number = 0;
		do {
			const toHash = input + number;
			const result = Md5.init(toHash);
			if (result.startsWith('00000')) {
				console.log(number);
				found = true;
			} else {
				number += 1;
			}
		} while (!found)

		this.setState({ md5Number: number });
		this.part2();
	}

	// https://adventofcode.com/2015/day/03
	async part2() {
		const input = "iwrupvqb";
		let found = false;
		let number = 0;
		do {
			const toHash = input + number;
			const result = Md5.init(toHash);
			if (result.startsWith('000000')) {
				console.log(number);
				found = true;
			} else {
				number += 1;
			}
		} while (!found)

		this.setState({ md5Number: number });		
	}
}