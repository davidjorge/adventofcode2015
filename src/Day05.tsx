import * as helpers from './helpers';
import React from 'react'

const day = '05';

interface DayState {
	niceStringsP1: number
	niceStringsP2: number,
	passedRepeating: string[],
	allLines: string[]
}

export default class Day extends React.Component {

	state: DayState = {
		niceStringsP1: 0,
		niceStringsP2: 0,
		passedRepeating: [],
		allLines: [],
	};

	render() {
		const middleFilter = this.state.allLines.filter(line => this.twoRepeatedLettersWithOneInTheMiddle(line));
		const twoPairs = this.state.allLines.filter(line => this.twoPairsRepeated(line));
		const both = twoPairs.filter(line => this.twoRepeatedLettersWithOneInTheMiddle(line));
		return (
			<div>
				<div>Answers: 68</div>
				<div>Number of lines {this.state.allLines.length}</div>
				<div>With 3 and 1 in the middle {middleFilter.length}</div>
				<div>Two pairs repeated {twoPairs.length}</div>
				<div>Both {both.length}</div>

				<div>Part 1: Nice strings P1 {this.state.niceStringsP1}</div>
				<div>Part 2: Nice Length P2 {this.state.niceStringsP2}</div>
			</div>
		);
	}

	componentDidMount() {
		// this.part1();
		this.part2();
	}

	private threeOrMoreVowels(line: string): boolean {
		let regExp = /a|e|i|o|u/g;
		let result = line.match(regExp);
		if (result) {
			return result.length >= 3;
		} else {
			return false;
		}
	}

	private twoRepeatedLetters(line: string): boolean {
		let lastLetter = '';
		return [...line].some(letter => {
			const repeated = letter === lastLetter;
			lastLetter = letter;
			return repeated;
		});
	}

	private containsForbiddenPairs(line: string): boolean {
		return ["ab", "cd", "pq", "xy"].some((pair) => line.indexOf(pair) !== -1);
	}

	// https://adventofcode.com/2015/day/05
	async part1() {
		const lines = await helpers.readLines(day)
		let niceCounter = 0;
		lines.forEach(line => {
			let passed = this.threeOrMoreVowels(line);
			passed = passed && this.twoRepeatedLetters(line);
			passed = passed && !this.containsForbiddenPairs(line);
			if (passed) {
				niceCounter += 1;
			}
		});
		this.setState({ niceStringsP1: niceCounter });
	}


	//---------------------------------------------------------------
	// Part 2 functions
	//---------------------------------------------------------------
	private twoPairsRepeated(line: string): string | undefined {
		let pairs: { [pair: string]: number } = {};
		let repeatedPair = undefined;
		[...line].some((letter, index) => {
			if (index === line.length - 1) {
				return false;
			}
			const nextLetter = line[index + 1];
			const pair = letter + nextLetter;
			if (pairs[pair] !== undefined) {
				if (index - 1 !== pairs[pair]) {
					repeatedPair = pair;
					return true;
				}
			} else {
				pairs[pair] = index;
			}
			return false;
		});
		return repeatedPair;
	}

	private twoRepeatedLettersWithOneInTheMiddle(line: string): string | undefined {
		let middle = undefined;
		[...line].some((letter, index) => {
			if (index + 2 >= line.length) {
				return false;
			}
			let nextNextLetter = line[index + 2];
			let repeated = letter === nextNextLetter;
			if (repeated) {
				middle = letter + line[index + 1] + nextNextLetter;
			}
			return repeated;
		});
		return middle;
	}

	private isNice(line: string): boolean {
		const repeatedPair = this.twoPairsRepeated(line);
		const middle = this.twoRepeatedLettersWithOneInTheMiddle(line);
		return repeatedPair !== undefined && middle !== undefined;
	}

	// https://adventofcode.com/2015/day/05
	async part2() {
		const linesDown = await helpers.readLines(day, false)
		const lines = linesDown;
		let niceCounter = 0;
		let passedRepeating: string[] = [];
		lines.forEach(line => {
			if (this.isNice(line)) {
				niceCounter += 1;
				passedRepeating.push(line)
			}
		});
		this.setState({ niceStringsP2: niceCounter, passedRepeating: passedRepeating, allLines: lines });
	}
}